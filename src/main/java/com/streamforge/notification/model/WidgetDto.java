package com.streamforge.notification.model;

public class WidgetDto {

    private String widgetToken;
    private Long id;
    private Long userId;
    private Long userExternalId;

    public String getWidgetToken() {
        return widgetToken;
    }

    public void setWidgetToken(String widgetToken) {
        this.widgetToken = widgetToken;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserExternalId() {
        return userExternalId;
    }

    public void setUserExternalId(Long userExternalId) {
        this.userExternalId = userExternalId;
    }
}
