package com.streamforge.notification.service;

import com.streamforge.http.request.SimpleRequestWrapper;
import com.streamforge.notification.model.TwitchNotificationMessage;
import com.streamforge.notification.model.WidgetDto;
import com.streamforge.realm.twitch.oauth.builder.TwitchHttpQueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

import java.util.HashMap;
import java.util.Map;

@Service
public class NotificationService {

    @Value("${streamforge.twitch.client-id}")
    private String clientId;

    @Autowired
    private SimpMessagingTemplate template;

    @Autowired
    private TwitchHttpQueryBuilder twitchHttpQueryBuilder;

    private Map<String, WidgetDto> sessions = new HashMap<>();

    public void subscribe(WidgetDto widgetDto) {
        if (!verifyWidgetToken(widgetDto.getWidgetToken())) {
            SimpleRequestWrapper<MultiValueMap<String, String>, String> wrapper = new SimpleRequestWrapper<>();
            wrapper.setBody(twitchHttpQueryBuilder.subscribeToTopic(widgetDto.getUserExternalId(), widgetDto.getWidgetToken()))
                    .addHeader("Client-ID", clientId)
                    .setUrl("https://api.twitch.tv/helix/webhooks/hub")
                    .setContentType(MediaType.APPLICATION_JSON);
            ResponseEntity<String> response = wrapper.exchange(HttpMethod.POST, String.class);
            if (response.getStatusCode() == HttpStatus.ACCEPTED) {
                sessions.put(widgetDto.getWidgetToken(), widgetDto);
                template.convertAndSend("/twitch-notif/follow/" + widgetDto.getWidgetToken(), response.getBody());
                template.convertAndSend("/twitch-notif/userpage/follow/"
                        + widgetDto.getUserId() + "/" + widgetDto.getId(), response.getBody());
            }
        }
    }

    public boolean verifyWidgetToken(String token) {
        return sessions.containsKey(token);
    }

}
