package com.streamforge.resource.web;

public class ResourceDefinitions {
    private static final String APP_PREFIX = "/api";

    //paths
    public static final String AUTH_PATH = APP_PREFIX + "/resource";
    public static final String INTEGRATION_PATH = APP_PREFIX + "/integration";

    private static final String NOTIF_PREFIX = APP_PREFIX + "/notification";

    public static final String NOTIFICATION_SUBSCRIBE_PATH = NOTIF_PREFIX + "/subscribe";

    public static final String NOTIFICATION_USER_FOLLOWS = NOTIF_PREFIX + "/follows/{widgetToken}";

    public static final int TOKEN_EXPIRATION_TIME = 60 * 60 * 24 * 10;

}
