package com.streamforge.resource.web.resource;

import com.streamforge.notification.service.NotificationService;
import com.streamforge.realm.twitch.oauth.builder.TwitchHttpQueryBuilder;
import com.streamforge.realm.twitch.oauth.service.TwitchAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.net.URISyntaxException;

import static com.streamforge.resource.web.ResourceDefinitions.*;

@Controller
public class AuthController {

    @Value("${streamforge.web.root}")
    private String webRoot;

    private final TwitchHttpQueryBuilder twitchHttpQueryBuilder;
    private final TwitchAuthService twitchAuthService;
    private final NotificationService notificationService;

    @Autowired
    public AuthController(TwitchHttpQueryBuilder twitchHttpQueryBuilder,
                          TwitchAuthService twitchAuthService,
                          NotificationService notificationService) {
        this.twitchHttpQueryBuilder = twitchHttpQueryBuilder;
        this.twitchAuthService = twitchAuthService;
        this.notificationService = notificationService;
    }

    @RequestMapping(value = AUTH_PATH, method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView authTwitch() throws URISyntaxException {
        return new ModelAndView("redirect:" + twitchHttpQueryBuilder.createAuthURI());
    }

    @RequestMapping(value = INTEGRATION_PATH, method = RequestMethod.GET)
    @ResponseBody
    public RedirectView twitchCallback(@RequestParam("code") String code,
                                       @RequestParam("state") String state,
                                       @RequestParam("scope") String scope,
                                       HttpServletResponse response) {
        String token = twitchAuthService.processCallback(code, state);
        if (token != null) {
            Cookie cookie = new Cookie("token", token.replaceAll("\"", ""));
            cookie.setMaxAge(TOKEN_EXPIRATION_TIME);
            cookie.setPath("/");
            response.addCookie(cookie);
        }
        return new RedirectView(webRoot);
    }

    @RequestMapping(value = NOTIFICATION_USER_FOLLOWS, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> twitchUserFollowVerify(@PathVariable("widgetToken") String widgetToken,
                                                         @RequestParam(value = "hub.challenge") String hubMode) throws Exception {
        return ResponseEntity.ok(hubMode);
    }

    @RequestMapping(value = NOTIFICATION_USER_FOLLOWS, method = RequestMethod.POST)
    @ResponseBody
    public void twitchUserFollowCallback(@PathVariable("widgetToken") String widgetToken,
                                         @RequestBody String payload) throws Exception {
        if (notificationService.verifyWidgetToken(widgetToken)) {
            throw new Exception();
        }
    }

}
