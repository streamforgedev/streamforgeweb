package com.streamforge.resource.web.resource;

import com.streamforge.notification.model.WidgetDto;
import com.streamforge.notification.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import static com.streamforge.resource.web.ResourceDefinitions.NOTIFICATION_SUBSCRIBE_PATH;

@Controller
public class NotificationController {

    @Autowired
    private NotificationService notificationService;

    @RequestMapping(value = NOTIFICATION_SUBSCRIBE_PATH, method = RequestMethod.POST)
    @ResponseBody
    public void subscribe(@RequestBody WidgetDto widget) {
        notificationService.subscribe(widget);
    }

}
