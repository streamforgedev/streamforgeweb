package com.streamforge.realm.twitch.oauth.service.impl;

import com.streamforge.http.request.SimpleRequestWrapper;
import com.streamforge.realm.twitch.oauth.builder.TwitchHttpQueryBuilder;
import com.streamforge.realm.twitch.oauth.service.TwitchAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

import java.util.Objects;

import static com.streamforge.resource.web.RestDefinitions.SERVICES_SAVE_PATH;

@Service
public class TwitchAuthServiceImpl implements TwitchAuthService {

    @Value("${streamforge.twitch.token-uri}")
    private String tokenURI;

    @Value("${streamforge.services.savepath}")
    private String serviceSavePath;

    private final TwitchHttpQueryBuilder twitchHttpQueryBuilder;

    @Autowired
    public TwitchAuthServiceImpl(TwitchHttpQueryBuilder twitchHttpQueryBuilder) {
        this.twitchHttpQueryBuilder = twitchHttpQueryBuilder;
    }

    @Override
    public String processCallback(String code, String status) {
        String token = requestForToken(twitchHttpQueryBuilder.createTokenRequestURI(code));
        return token != null ? saveTokenRequest(token) : null;
    }

    private String requestForToken(MultiValueMap<String, String> params) {
        SimpleRequestWrapper<MultiValueMap<String, String>, String> wrapper = new SimpleRequestWrapper<>();
        wrapper.setBody(params)
                .setUrl(tokenURI)
                .setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        ResponseEntity<String> response = wrapper.exchange(HttpMethod.POST, String.class);
        return response.getStatusCode() == HttpStatus.OK ? response.getBody() : null;
    }

    private String saveTokenRequest(String body) {
        SimpleRequestWrapper<String, String> wrapper = new SimpleRequestWrapper<>();
        wrapper.setBody(body)
                .setUrl(serviceSavePath + SERVICES_SAVE_PATH)
                .setContentType(MediaType.APPLICATION_JSON);
        ResponseEntity<String> response = wrapper.exchange(HttpMethod.POST, String.class);
        return response.getStatusCode() == HttpStatus.OK
                ? String.valueOf(Objects.requireNonNull(response.getHeaders().get("Authorization")).get(0))
                : null;
    }

}
