import React from "react";
import Select from "react-dropdown-select";

import NavList from "../../dashboard/nav-list/nav-list.js"

import styleFont from "../../fonts/css/open-sans.css"
import style from "bulma/css/bulma.min.css"
import styleSelf from "./widget-container.css"

import Config from "../../config/config";
import AppLoader from "../../commons/loader/loader";
import WidgetItem from "../widget-item/widget-item";
import {toast, ToastContainer} from "react-toastify";

import cuphead1 from "../../../media/images/events/cuphead/cuphead1.gif"
import cuphead2 from "../../../media/images/events/cuphead/cuphead2.gif"
import cuphead3 from "../../../media/images/events/cuphead/cuphead3.gif"
import cuphead4 from "../../../media/images/events/cuphead/cuphead4.gif"
import cuphead5 from "../../../media/images/events/cuphead/cuphead5.gif"
import cuphead6 from "../../../media/images/events/cuphead/cuphead6.gif"

import fortnite1 from "../../../media/images/events/fortnut/fortnite1.gif"
import fortnite2 from "../../../media/images/events/fortnut/fortnite2.gif"

import kart1 from "../../../media/images/events/mariokart/kart1.gif"
import kart2 from "../../../media/images/events/mariokart/kart2.gif"
import kart3 from "../../../media/images/events/mariokart/kart3.gif"
import kart4 from "../../../media/images/events/mariokart/kart4.gif"

import mgs from "../../../media/images/events/mgs/mgs1.gif"

import logo from "../../../media/images/twitch-logo.png"

const imagesConfig = {
    "MGS" : [mgs],
    "MARIOKART": [kart1, kart2, kart3, kart4],
    "CUPHEAD": [cuphead1, cuphead2, cuphead3, cuphead4, cuphead5, cuphead6],
    "FORTNITE": [fortnite1, fortnite2]
};

class WidgetContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fetched: false,
            createMode: false
        };

        this.onWidgetDelete = this.onWidgetDelete.bind(this);
    }

    onWidgetDelete() {
        this.setState({fetched: false});
    }

    componentDidMount() {
        if (!this.state.fetched) {
            fetch(Config.BASE_REST_URL + "/api/safe/widget/all", Config.defaultOptions())
                .then((response) => response.json())
                .then((response) => {
                    this.data = response;
                    this.setState({fetched: true})
                });
        }
    }

    componentDidUpdate() {
        if (!this.state.fetched) {
            fetch(Config.BASE_REST_URL + "/api/safe/widget/all", Config.defaultOptions())
                .then((response) => response.json())
                .then((response) => {
                    this.data = response;
                    this.setState({fetched: true})
                });
        }
    }

    render() {
        const defineType = (value) => {
            return this.props.widgetType ? this.props.widgetType === value : true;
        };

        if (this.state.fetched) {
            if (this.state.createMode) {
                return (
                    <div className="container">
                        <div className="columns">
                            <div className="column is-3">
                                <NavList nav={this.props.nav}/>
                            </div>
                            <div className="column is-9 widget-con">
                                <WidgetCreateMode saveCallback={() => this.setState({createMode: false, fetched: false})}/>
                            </div>
                        </div>
                    </div>
                );
            } else {
                return (
                    <div className="container">
                        <div className="columns">
                            <div className="column is-3">
                                <NavList nav={this.props.nav}/>
                            </div>
                            <div className="column is-9 widget-con">
                                <div className="button is-fullwidth is-info is-outlined"
                                     onClick={() => this.setState({createMode: true})}>
                                    Create new widget
                                </div>
                                {
                                    this.data
                                        .filter(item => defineType(item.type))
                                        .map(item => <WidgetItem onWidgetDelete={this.onWidgetDelete} {...item} />)
                                }
                            </div>
                        </div>
                    </div>
                );
            }
        } else {
            return (
                <AppLoader />
            );
        }
    }
}

class WidgetCreateMode extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            appearance: "",
            type: ""
        };

        this.onSave = this.onSave.bind(this);
    }

    onSave() {
        if (this.state.type) {
            fetch(Config.BASE_REST_URL + "/api/safe/widget", {
                ...Config.defaultOptions(),
                method: "POST",
                body: JSON.stringify({appearance: this.state.appearance.value, type: this.state.type.value})
            }).then(() => {
                    toast.success('Widget was successfully created', {
                        position: "bottom-left",
                        autoClose: 2000,
                        hideProgressBar: false
                    })
                })
                .catch(() => {})
                .finally(() => {this.props.saveCallback()})
        }
    }

    render() {
        const types = [
            {
                name: "Chat",
                value: "CHAT"
            },
            {
                name: "Bits",
                value: "BITS"
            },
            {
                name: "Whispers",
                value: "WHISPERS"
            },
            {
                name: "Subscribe",
                value: "SUB"
            }
        ];

        const styles = [
            {
                name: "Kettle Beer Squalid",
                value: "MGS"
            },
            {
                name: "Luigi`s Racing",
                value: "MARIOKART"
            },
            {
                name: "MugMan",
                value: "CUPHEAD"
            },
            {
                name: "Fortnut",
                value: "FORTNITE"
            }
        ];

        const apValue = this.state.appearance;

        return (
            <div>
                <div className="container widget-create-container">
                    <div className="columns">
                        <div className="column is-narrow">
                            <figure className="image is-128x128">
                                <img src={
                                    apValue && apValue.value
                                        ? imagesConfig[apValue.value][Math.floor(Math.random() * imagesConfig[apValue.value].length)]
                                        : logo
                                } className="widget-image" />
                            </figure>
                        </div>

                        <div className="column is-narrow">
                            <div className="dropdown widget-dd-spacing">
                                <Select searchable="false" color="#FFFFFF" labelField="name" valueField="value" options={types}
                                        onChange={(values) => this.setState({type: values[0]})} />
                            </div>
                            <br />

                        {
                            this.state.type && this.state.type.value !== "CHAT"
                            ? <div className="dropdown widget-dd-spacing">
                                <Select searchable="false" color="#FFFFFF" labelField="name" valueField="value" options={styles}
                                        onChange={(values) => this.setState({appearance: values[0]})} />
                                </div>
                            : null
                        }

                        </div>

                    </div>
                </div>
                <div className="button is-fullwidth is-info is-outlined"
                     onClick={this.onSave}>
                    Create
                </div>
                <ToastContainer position={toast.POSITION.BOTTOM_LEFT} />
            </div>
        );
    }
}

export default WidgetContainer;