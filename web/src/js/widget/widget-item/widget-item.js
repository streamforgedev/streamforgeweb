import React from "react";
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import styleFont from "../../fonts/css/open-sans.css"
import style from "bulma/css/bulma.min.css"
import styleSelf from "./widget-item.css"

import Config from "../../config/config";
import mgs from "../../../media/images/events/mgs/mgs1.gif";
import kart1 from "../../../media/images/events/mariokart/kart1.gif";
import kart2 from "../../../media/images/events/mariokart/kart2.gif";
import kart3 from "../../../media/images/events/mariokart/kart3.gif";
import kart4 from "../../../media/images/events/mariokart/kart4.gif";
import cuphead1 from "../../../media/images/events/cuphead/cuphead1.gif";
import cuphead2 from "../../../media/images/events/cuphead/cuphead2.gif";
import cuphead3 from "../../../media/images/events/cuphead/cuphead3.gif";
import cuphead4 from "../../../media/images/events/cuphead/cuphead4.gif";
import cuphead5 from "../../../media/images/events/cuphead/cuphead5.gif";
import cuphead6 from "../../../media/images/events/cuphead/cuphead6.gif";
import fortnite1 from "../../../media/images/events/fortnut/fortnite1.gif";
import fortnite2 from "../../../media/images/events/fortnut/fortnite2.gif";
import logo from "../../../media/images/twitch-logo.png";

const imagesConfig = {
    "MGS" : [mgs],
    "MARIOKART": [kart1, kart2, kart3, kart4],
    "CUPHEAD": [cuphead1, cuphead2, cuphead3, cuphead4, cuphead5, cuphead6],
    "FORTNITE": [fortnite1, fortnite2]
};

class WidgetItem extends React.Component {
    constructor(props) {
        super(props);

        this.onRemove = this.onRemove.bind(this);
    }

    onRemove() {
        fetch(Config.BASE_REST_URL + `/api/safe/widget/status/${this.props.id}`, {...Config.defaultOptions(), method: "DELETE"})
            .finally(() => { this.props.onWidgetDelete() });
    }

    render() {
        const ap = this.props.appearance && imagesConfig[this.props.appearance]
            ? imagesConfig[this.props.appearance][Math.floor(Math.random() * imagesConfig[this.props.appearance].length)]
            : logo;

        return (
            <div className="container item-container">
                <div className="columns">
                    <div className="column is-narrow">
                        <figure className="image is-128x128">
                            <img src={ap} className="widget-image"/>
                        </figure>
                    </div>
                    <div className="column is-narrow">
                        <div className="tags has-addons">
                            <span className="tag">Active</span>
                            <span className={this.props.active ? "tag is-success" : "tag is-danger"}>
                                {this.props.active ? "Yes" : "No"}
                            </span>
                        </div>
                        <div className="tags has-addons">
                            <span className="tag is-info">Theme</span>
                            <span className="tag is-light">{this.props.appearance}</span>
                        </div>
                        <div className="tags has-addons">
                            <span className="tag is-primary">Type</span>
                            <span className="tag is-black">{this.props.type}</span>
                        </div>
                    </div>
                    <div className="column">
                        <input className="input widget-input" type="text"
                               value={Config.BASE_WEB_URL + `/widget/${this.props.widgetToken}/user/${this.props.userId}`}
                               readOnly />
                    </div>
                    <div className="column is-narrow widget-tag">
                        <CopyToClipboard text={Config.BASE_WEB_URL + `/widget/${this.props.widgetToken}/user/${this.props.userId}`}
                                         onCopy={() =>  toast.success('Copied to the clipboard', {
                                             position: "bottom-left",
                                             autoClose: 2000,
                                             hideProgressBar: false
                                         })}>
                            <div className="button is-small is-success widget-button is-outlined">Copy URL</div>
                        </CopyToClipboard>
                        <br />
                        <div className="button is-small is-danger is-outlined widget-button" onClick={this.onRemove}>Delete</div>
                    </div>
                </div>
                <ToastContainer position={toast.POSITION.BOTTOM_LEFT} />
            </div>
        );
    }
}

export default WidgetItem;