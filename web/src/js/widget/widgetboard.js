import React, {Fragment} from "react";
import ReactDOM from "react-dom";

import AuthComponent from "../security/auth-component";
import Navigation from "../dashboard/navigation/navigation";
import WidgetContainer from "./widget-container/widget-container";

class InternalWidgetBoard extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Fragment>
                <Navigation />
                <WidgetContainer {...this.props} />
            </Fragment>
        )
    }
}

class WidgetBoard extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <AuthComponent component = {
                <InternalWidgetBoard {...this.props} widgetType={this.props.type} />
            } />
        )
    }
}

export default WidgetBoard;