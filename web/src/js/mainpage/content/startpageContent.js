import React from "react";
import Config from "../../config/config";

import "./startpage-content.css";
import StreamItem from "./streamItem";

class StartContent extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			data: []
		};

		this.loadStreams();
	}

	loadStreams() {
		const component = this;
		fetch(Config.BASE_REST_URL + Config.restConfig().getStreams, {method: "GET"})
			.then((data) => data.json())
			.then((data) => {
				component.setState({data: data.data});
			})
			.catch((error) => {
			});
	}

	render() {
		return (
			<div style={{
				margin: 0,
				padding: 0,
				background: 'rgba(0, 0, 0, 0) !important'
            }}>
				<div className="content"/>
				<div>
					{
						(this.state.data && this.state.data.length)
						? this.state.data.map((stream) => <StreamItem streamData={stream}/>)
						: null
					}
				</div>
            </div>
		);
	}
}

export default StartContent;
