import React from "react";

import "./streamItem.css";
import Player from "../../commons/player/player";
import Utility from "../../config/utils/Utility";

class StreamItem extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		const username = Utility.retrieveStreamerName(this.props.streamData.thumbnailUrl);

		console.log(username);

		return (
				<div className="streamItemContainer">
                    <Player channel={username} ratio={ 16 / 8 } />
				</div>
		);
	}
}

export default StreamItem;
