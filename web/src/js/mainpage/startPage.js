import React from "react";
import $ from "jquery";
import Progress from "react-progress-2";

import StartHeader from "./header/startpageHeader.js";
import StartPromo from "./promo/startpagePromo.js";
import StartContent from "./content/startpageContent";

import favicon from "../../media/images/sword-icon-20.png";
import Config from "../config/config";
import queryString from "query-string";
import AppLoader from "../commons/loader/loader";

class StartPage extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			fetched: false
		};
	}

    componentWillMount() {
       	if (queryString.parse(this.props.location.search).state !== 'unauthorized') {
       		this.props.history.block();
            window.location.replace(Config.BASE_WEB_URL + "/dashboard");
        } else {
        	this.setState({ fetched: true });
		}
	}

	componentDidMount() {
		$("link[rel*='icon']").attr("href", favicon);
	}

	render() {
		if (this.state.fetched) {
            return (
                <React.Fragment>
                    <Progress.Component/>
                    <StartHeader/>
                    <StartPromo/>
                    <StartContent/>
                </React.Fragment>
            );
        } else {
            return (
                <AppLoader />
            );
		}
	}
}

export default StartPage;
