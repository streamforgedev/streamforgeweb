import React from "react";
import { animateScroll } from "react-scroll";
import Highlighter from "react-highlight-words";
import queryString from 'query-string'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import BotClient from "../../config/twitchclient";

import styleSelf from "./chat.css";

import audio from "../../../media/sound/intuition.mp3";
import Config from "../../config/config";
import Utility from "../../config/utils/Utility";

class Chat extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            messages: [],
            allowDelete: false
        };

        this.onMessageReceived = this.onMessageReceived.bind(this);
        this.scrollToBottom = this.scrollToBottom.bind(this);

        this.login = this.props.login;

        this.client = new BotClient(this.props.login, this.onMessageReceived, () => {});
    }

    componentDidUpdate() {
        if (this.state.allowDelete) {
            let messages = [...this.state.messages];
            messages.shift();
            this.setState({messages: messages, allowDelete: false}, this.scrollToBottom);
        }
    }

    onMessageReceived(target, context, msg, self) {
        if (self) { return; }

        const commandName = msg.trim();

        if (commandName.indexOf(`@${this.login}`) !== -1) {
            console.log(audio);
            new Audio(Config.BASE_WEB_URL + audio).play();
        }

        if (commandName === '!dice') {
            console.log(`* Executed ${commandName} command`);
        } else {
            this.createMessage(commandName, "is-message", context.username, context);
        }
    }

    createMessage(message, messageClass, userName, context) {
        let msg = {
            message: message,
            messageClass: `notification ${messageClass} chat-message`,
            user: `${userName}: `,
            badge: Utility.getUserBadge(context.badges)
        };

        let messages = [...this.state.messages, msg];

        this.setState({messages: messages, allowDelete: messages.length === 20}, this.scrollToBottom);
    }

    scrollToBottom() {
        animateScroll.scrollToBottom({
            containerId: "chat-container"
        });
    }

    render() {
        const badge = (messageItem) => {
            console.log(messageItem.badge);
            return messageItem.badge
                ? <FontAwesomeIcon icon={messageItem.badge} />
                : null
        };

        return (
            <ul className="chat-container" id="chat-container">
                {
                    this.state.messages.map(messageItem =>
                        <li className={messageItem.messageClass}>
                            {
                                badge(messageItem)
                            }
                            &nbsp;
                            <span className="chat-username">{messageItem.user}</span>
                            &nbsp;
                            <Highlighter
                                highlightClassName="chat-self-msg-text"
                                searchWords={ [`@${this.login}`] }
                                autoEscape={true}
                                textToHighlight={messageItem.message}
                            />
                        </li>
                    )
                }
            </ul>
        )
    }
}

export default Chat;