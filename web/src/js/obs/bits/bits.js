import React from "react";

import mgs from "../../../media/images/events/mgs/mgs1.gif";

import kart1 from "../../../media/images/events/mariokart/kart1.gif";
import kart2 from "../../../media/images/events/mariokart/kart2.gif";
import kart3 from "../../../media/images/events/mariokart/kart3.gif";
import kart4 from "../../../media/images/events/mariokart/kart4.gif";

import cuphead1 from "../../../media/images/events/cuphead/cuphead1.gif";
import cuphead2 from "../../../media/images/events/cuphead/cuphead2.gif";
import cuphead3 from "../../../media/images/events/cuphead/cuphead3.gif";
import cuphead4 from "../../../media/images/events/cuphead/cuphead4.gif";
import cuphead5 from "../../../media/images/events/cuphead/cuphead5.gif";
import cuphead6 from "../../../media/images/events/cuphead/cuphead6.gif";

import fortnite1 from "../../../media/images/events/fortnut/fortnite1.gif";
import fortnite2 from "../../../media/images/events/fortnut/fortnite2.gif";

import {ToastContainer, toast} from "react-toastify";
import PubSub from "../../config/pubsub";
import AppLoader from "../../commons/loader/loader";

const imagesConfig = {
    "MGS" : [mgs],
    "MARIOKART": [kart1, kart2, kart3, kart4],
    "CUPHEAD": [cuphead1, cuphead2, cuphead3, cuphead4, cuphead5, cuphead6],
    "FORTNITE": [fortnite1, fortnite2]
};

class BitsCheer extends React.Component {
    constructor(props) {
        super(props);

        this.onMessageReceive = this.onMessageReceive.bind(this);

        this.client = new PubSub(this.props.pubSubSubscriptionDto, this.onMessageReceive);
    }

    onMessageReceive(data) {
        toast.info(<BitsCheerToast appearance={this.props.widgetDto.appearance} {...data}/>, {autoClose: 4000})
    }

    render() {
        return (
            <ToastContainer newestOnTop position={toast.POSITION.TOP_LEFT} />
        );
    }
}

class BitsCheerToast extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        if (this.props.appearance && imagesConfig[this.props.appearance]) {
            return (
                <div className="container t-body">
                    <div className="columns">
                        <div className="column is-narrow">
                            <figure className="image is-64x64 toast-image">
                                <img
                                    src={imagesConfig[this.props.appearance][Math.floor(Math.random() * imagesConfig[this.props.appearance].length)]}/>
                            </figure>
                        </div>
                        <div className="column">
                            <br />
                            <p>
                                {
                                    `${this.props.is_anonymous ? "Stranger" : this.props.user_name} cheered you with ${this.props.bits_used} bits!`
                                }
                            </p>
                        </div>
                    </div>
                </div>
            );
        } else {
            return (<AppLoader/>);
        }
    }
}

export default BitsCheer;