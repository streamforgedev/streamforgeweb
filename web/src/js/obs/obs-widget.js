import React from "react";

import styleFont from "../fonts/css/open-sans.css"
import style from "bulma/css/bulma.min.css"
import styleSelf from "./obs-widget.css"

import Config from "../config/config";

import AppLoader from "../commons/loader/loader";
import Chat from "./chat/chat";
import Sub from "./sub/sub";
import Whisper from "./whispers/whispers";
import BitsCheer from "./bits/bits";

class OBSWidget extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fetched: false
        };
    }

    componentDidMount() {
        if (!this.state.fetched) {
            const component = this;
            fetch(Config.BASE_REST_URL + `/api/widget/${this.props.match.params.userId}/token/${this.props.match.params.token}`)
                .then((response) => response.json())
                .then((response) => {
                    component.data = response;
                    component.setState({fetched: true})
                })
        }
    }


    render() {
        if (this.state.fetched) {
            switch (this.data.widgetDto.type) {
                case "WHISPERS":
                    return (<Whisper {...this.data} />);
                case "BITS":
                    return (<BitsCheer {...this.data}/>);
                case "SUB":
                    return (<Sub {...this.data} />);
                case "CHAT":
                    return (<Chat login={this.data.widgetDto.topic} />);
            }
            return (<AppLoader/>);
        } else {
            return (<AppLoader/>);
        }
    }
}

export default OBSWidget;