import React from "react";
import Config from "../config/config";

import AppLoader from "../commons/loader/loader";

class AuthComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            render: false,
            userData: []
        };
    }

    componentDidMount() {
        const component = this;
        const headers = Config.defaultOptions();

        if (!headers.headers) {
            window.location.replace(Config.BASE_WEB_URL + "?state=unauthorized");
        }

        fetch(Config.BASE_REST_URL + Config.restConfig().getUserInfoPath, headers)
            .then((response) => response.json())
            .then((response) => {
                fetch(Config.BASE_REST_URL + Config.restConfig().getTwitchUserInfoPath, headers)
                    .then((TwitchResponse) => TwitchResponse.json())
                    .then((TwitchResponse) => {
                        component.state.userData.push({...response, ...TwitchResponse});
                        component.setState({ render : true })
                    })
                    .catch(() => {
                        window.location.replace(Config.BASE_WEB_URL + "?state=unauthorized");
                    });
            })
            .catch(() => {
                window.location.replace(Config.BASE_WEB_URL + "?state=unauthorized");
            });
    }

    render() {
        let component = this.state.render ? this.props.component : null;

        if (component != null) {
            component = React.cloneElement(
                component,
                {...this.state.userData[0]}
            );

            return (
                component
            );
        } else {
            return (
                <AppLoader />
            );
        }
    }
}

export default AuthComponent;