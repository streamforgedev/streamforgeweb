import React, {Fragment} from "react";

import PageHero from "../../commons/page-hero/page-hero";
import NumberCard from "../../commons/number-card/number-card";
import AppLoader from "../../commons/loader/loader";
import Player from "../../commons/player/player";

import styleSelf from "./stats.css"
import Config from "../../config/config";
import Utility from "../../config/utils/Utility";
import PubSub from "../../config/pubsub";

class Stats extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fetched: false
        };
    }

    componentDidMount() {
        if (!this.state.fetched) {
            fetch(Config.BASE_REST_URL + Config.restConfig().getTwitchUserFullInfoPath, Config.defaultOptions())
                .then((response) => response.json())
                .then((response) => {
                    this.data = response;
                    this.setState({fetched: true})
                });
        }
    }

    render() {
        if (this.state.fetched) {
            console.log(this.props);
            console.log(this.data);

            return (
                null
            );
        } else {
            return (
                <AppLoader />
            );
        }
    }
}

export default Stats;