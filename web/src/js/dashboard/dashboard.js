import React, {Fragment} from "react";
import ReactDOM from "react-dom";

import AuthComponent from "../security/auth-component";

import Navigation from "./navigation/navigation"
import DashboardContainer from "./dashboard-container/dashboard-container";
import PubSub from "../config/pubsub";

class InternalDashboard extends React.Component {
    constructor(props) {
        super(props);
        this.c = new PubSub();
    }

    render() {
        return (
            <Fragment>
                <Navigation />
                <DashboardContainer {...this.props} />
            </Fragment>

        )
    }
}

class Dashboard extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <AuthComponent component = {
                <InternalDashboard {...this.props} />
            } />
        )
    }
}

export default Dashboard;