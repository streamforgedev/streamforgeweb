import React from "react";

import style from "bulma/css/bulma.min.css"
import styleSelf from "./navigation.css"

class Navigation extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <nav className="navbar is-white">
                <div className="container">
                    <div className="navbar-brand">
                        <a className="navbar-item brand-text" href="#">
                            StreamForge
                        </a>
                        <div className="navbar-burger burger" data-target="navMenu">
                            <span />
                            <span />
                            <span />
                        </div>
                    </div>
                    <div id="navMenu" className="navbar-menu">
                        <div className="navbar-start">
                            <a className="navbar-item" href="/dashboard">
                                Dashboard
                            </a>
                            <a className="navbar-item" href="/widgets">
                                Widgets
                            </a>
                            <a className="navbar-item" href="#">
                                Streams
                            </a>
                        </div>
                    </div>
                </div>
            </nav>
        );
    }
}

export default Navigation;