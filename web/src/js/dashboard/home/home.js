import React, {Fragment} from "react";

import PageHero from "../../commons/page-hero/page-hero";
import NumberCard from "../../commons/number-card/number-card";
import AppLoader from "../../commons/loader/loader";
import Player from "../../commons/player/player";

import styleSelf from "./home.css"
import Config from "../../config/config";
import Utility from "../../config/utils/Utility";

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fetched: false
        };
    }

    componentDidMount() {
        if (!this.state.fetched) {
            fetch(Config.BASE_REST_URL + Config.restConfig().getTwitchUserFullInfoPath, Config.defaultOptions())
                .then((response) => response.json())
                .then((response) => {
                    this.data = response;
                    this.setState({fetched: true})
                });
        }
    }

    render() {
        if (this.state.fetched) {
            const tiles = [
                {
                    data: this.props.viewCount,
                    label: 'Viewers Total'
                },
                {
                    data: this.data.followers.total,
                    label: 'Followers Total'
                },
                {
                    data: this.data.followed.total,
                    label: 'Followed Total'
                },
                {
                    data: Utility.sumBitsTotals(this.data.bits.data),
                    label: 'Total Bits Score'
                }
            ];

            return (
                <Fragment>
                    <PageHero title={this.props.title} subtitle={this.props.subtitle}/>
                    <NumberCard cards={tiles}/>
                    <Player channel={this.props.login} ratio={ 16 / 6 } />
                </Fragment>
            );
        } else {
            return (
                <AppLoader />
            );
        }
    }
}

export default Home;