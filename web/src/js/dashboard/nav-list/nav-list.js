import React from "react";

import style from "bulma/css/bulma.min.css"
import styleSelf from "./nav-list.css"

class NavList extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <aside className="menu is-hidden-mobile">
                {
                    this.props.nav.map(navSection =>
                        <div>
                        <p className="menu-label">{navSection.name}</p>
                        <ul className="menu-list">
                            {
                                navSection.child.map(navSectionChild =>
                                    <li>
                                        <a href={navSectionChild.url} className={navSectionChild.isActive ? 'is-active' : null}>
                                            {navSectionChild.name}
                                        </a>
                                    </li>
                                )
                            }
                        </ul>
                        <br/>
                        </div>
                    )
                }
            </aside>
        );
    }
}

export default NavList;