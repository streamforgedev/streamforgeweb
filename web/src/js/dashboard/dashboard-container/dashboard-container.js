import React from "react";

import NavList from "../nav-list/nav-list.js"

import styleFont from "../../fonts/css/open-sans.css"
import style from "bulma/css/bulma.min.css"
import styleSelf from "./dashboard-container.css"

class DashboardContainer extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {children} = this.props;
        const properties = {...this.props, title: `Hi there ${this.props.login}`, subtitle: `Hi there ${this.props.login}`};

        return (
            <div className="container">
                <div className="columns">
                    <div className="column is-3">
                        <NavList nav={this.props.nav} />
                    </div>
                    <div className="column is-9">
                        {React.Children.map(children, child => {
                            return React.cloneElement(child, properties, null);
                        })}
                    </div>
                </div>
            </div>

        );
    }
}

export default DashboardContainer;