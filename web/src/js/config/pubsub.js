export default class PubSub {
    constructor(topics, onReceiveMessage) {
       this.socket = new WebSocket('wss://pubsub-edge.twitch.tv');

        this.socket.onopen = () => {
            console.log(topics);
            this.socket.send(JSON.stringify(topics));
            setInterval(() => {
                this.socket.send(JSON.stringify({type: 'PING'}))
                }, 60000 * 4);
        };

        this.socket.onmessage = function (event) {
            let data = JSON.parse(event.data);
            if (data.type && data.type === "MESSAGE") {
                console.log(JSON.parse(data.data.message));
                onReceiveMessage(JSON.parse(data.data.message).data_object);
            }
        }
    }

    get getSocket() {
        return this.socket;
    }
}