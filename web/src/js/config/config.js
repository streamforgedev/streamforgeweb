class Config {

	static get BASE_REST_WEB_URL() {
		return "http://localhost:7001";
	}

    static get BASE_REST_URL() {
        return "http://localhost:7002";
    }

	static get BASE_WEB_URL() {
        return "http://localhost:8080";
    }

    static restConfig = () => {
        return {
            authPath: "/api/resource",
            getStreams: "/api/streams/external",
            getUserInfoPath: "/api/safe/user",
            getTwitchUserInfoPath: "/api/safe/user/twitch",
            getTwitchUserFullInfoPath: "/api/safe/user/twitch/full"
        }
    };

    static logout(name) {
        document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    };

    static defaultOptions = () => {
        function getCookie(name) {
            let value = "; " + document.cookie;
            let parts = value.split("; " + name + "=");
            if (parts.length === 2) return parts.pop().split(";").shift();
        }

        return {
            headers: {
                'Authorization': getCookie("token") ? getCookie("token").split('"').join('') : null,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }
    };

}

export default Config;