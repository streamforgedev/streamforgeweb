const tmi = require('tmi.js');

const password = 'oauth:s49l58kl4s5w73rdmie771ghej6695';

class BotClient {
    constructor(username, onMessageReceive, onConnected) {
        console.log(username);
        this.client = new tmi.client({
            identity: {
                username: username,
                password: password
            },
            channels: [
                username
            ]
        });

        this.client.on('message', onMessageReceive);
        this.client.on('connected', onConnected);

        this.client.connect();
    }

    get getClient() {
        return this.client;
    }
}

export default BotClient;