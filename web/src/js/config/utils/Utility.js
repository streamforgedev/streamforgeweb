export default class Utility {

    static sumBitsTotals(array) {
        return array.map(item => item.score).reduce((a,b) => a + b, 0);
    }

    static retrieveStreamerName(url) {
        return url.split("/")
            .pop()
            .split("-")
            .shift()
            .replace("live_user_", "");
    }

    static getUserBadge(object) {
        let className = null;
        if (object) {
            if (object.broadcaster && object.broadcaster === '1') {
                className = "user-astronaut";
            } else if (object.moderator && object.moderator === '1') {
                className = "shield-alt";
            }
        }
        return className;
    }

}