import React from "react";
import ReactPlayer from 'react-player'
import Ratio from 'react-ratio';

import styleSelf from "./player.css"
import Config from "../../config/config";
import Utility from "../../config/utils/Utility";

class Player extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Ratio ratio={ this.props.ratio }>
                <ReactPlayer
                    url={`https://www.twitch.tv/${this.props.channel}`}
                    width='100%'
                    height='100%'
                    className='react-player'
                />
            </Ratio>
        );
    }
}

export default Player;