import React from "react";

import ReactCenter from "react-center";
import Loader from "react-loader-spinner";

import styleSelf from "./loader.css"

class AppLoader extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="center-all">
                <ReactCenter className="center">
                    <Loader
                        type="Oval"
                        color="#FFFFFF"
                        height="35"
                        width="35"
                    />
                </ReactCenter>
            </div>
        );
    }
}

export default AppLoader;