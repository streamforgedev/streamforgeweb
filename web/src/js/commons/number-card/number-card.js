import React from "react";

import styleSelf from "./number-card.css"

class NumberCard extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <section className="info-tiles">
                <div className="tile is-ancestor has-text-centered">
                    {this.props.cards.map(card =>
                        <div className="tile is-parent">
                            <article className="tile is-child box">
                                <p className="title">{card.data}</p>
                                <p className="subtitle">{card.label}</p>
                            </article>
                        </div>
                    )}
                </div>
            </section>

        );
    }
}

export default NumberCard;