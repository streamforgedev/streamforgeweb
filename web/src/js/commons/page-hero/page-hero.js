import React from "react";

import styleSelf from "./page-hero.css"

class PageHero extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <section className="hero is-info welcome is-small">
                <div className="hero-body">
                    <div className="container">
                        <h1 className="title">
                            {this.props.title}
                        </h1>
                        <h2 className="subtitle">
                            {this.props.subtitle}
                        </h2>
                    </div>
                </div>
            </section>
        );
    }
}

export default PageHero;