import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import StartPage from "./mainpage/startPage";
import Dashboard from "./dashboard/dashboard";

import Home from "./dashboard/home/home";
import Stats from "./dashboard/stats/stats";

import { library } from '@fortawesome/fontawesome-svg-core'
import { faShieldAlt, faUserAstronaut } from '@fortawesome/free-solid-svg-icons'
import WidgetBoard from "./widget/widgetboard";
import Config from "./config/config";
import OBSWidget from "./obs/obs-widget";

library.add(faShieldAlt, faUserAstronaut);

const homeDashboard = {
    name: 'Home',
    url: '/dashboard',
    isActive: false
};

const homeDashboardStats = {
    name: 'Stats',
    url: '/dashboard/stats',
    isActive: false
};

const homeDashboardGames = {
    name: 'Games',
    url: '/dashboard/games',
    isActive: false
};

const adminSettings = {
    name: 'Settings',
    url: '/dashboard/settings',
    isActive: false
};

const adminLogout = {
    name: 'Log Out',
    url: '/logout',
    isActive: false
};

/* Widgets */

const allWidgets = {
    name: 'All',
    url: '/widgets',
    isActive: false
};

const subWidgets = {
    name: 'Subscribe events',
    url: '/widgets/sub',
    isActive: false
};

const bitsWidgets = {
    name: 'Bits events',
    url: '/widgets/bits',
    isActive: false
};

const whisperWidgets = {
    name: 'Whispers',
    url: '/widgets/whisper',
    isActive: false
};

const chatWidgets = {
    name: 'Chats',
    url: '/widgets/chat',
    isActive: false
};

const allDBNav = [
	homeDashboard,
    homeDashboardStats,
    homeDashboardGames,
    adminSettings,
    adminLogout
];

const allWidgetsNav = [
    allWidgets,
    subWidgets,
    bitsWidgets,
    whisperWidgets,
    chatWidgets,
    adminLogout
];

const navDB = [
    {
        name: 'Widgets',
        child: [
            homeDashboard,
            homeDashboardStats,
            homeDashboardGames
        ]
    },
    {
        name: 'Administration',
        child: [
            adminSettings,
            adminLogout
        ]
    }
];

const navWidgets = [
    {
        name: 'Widgets',
        child: [
            allWidgets,
            subWidgets,
            bitsWidgets,
            whisperWidgets,
            chatWidgets
        ]
    },
    {
        name: 'Administration',
        child: [
            adminLogout
        ]
    }
];

class Index extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<BrowserRouter>
				<Switch>

					<Route exact path="/" component={ StartPage } />

					<Route exact path="/dashboard" component={() =>
                        <Dashboard nav={ enter(homeDashboard, allDBNav, navDB) }>
                            <Home />
                        </Dashboard>
					} />

					<Route exact path="/dashboard/stats" component={() =>
                        <Dashboard nav={ enter(homeDashboardStats, allDBNav, navDB) } >
                            <Stats />
                        </Dashboard>
					} />

                    <Route exact path="/dashboard/games" component={() =>
                        <Dashboard nav={ enter(homeDashboardGames, allDBNav, navDB) } />
                    } />

                    <Route exact path="/dashboard/settings" component={() =>
                        <Dashboard nav={ enter(adminSettings, allDBNav, navDB) } />
                    } />

                    <Route exact path="/logout" component={() => {
                            console.log('Entered /');
                            Config.logout("token");
                            window.location.replace(Config.BASE_WEB_URL + "?state=unauthorized");
                        }
                    } />

                    /* Widgets */

                    <Route exact path="/widgets" component={() =>
                        <WidgetBoard nav={ enter(allWidgets, allWidgetsNav, navWidgets) } />
                    } />

                    <Route exact path="/widgets/chat" component={() =>
                        <WidgetBoard nav={ enter(chatWidgets, allWidgetsNav, navWidgets) } type={"CHAT"} />
                    } />

                    <Route exact path="/widgets/sub" component={() =>
                        <WidgetBoard nav={ enter(subWidgets, allWidgetsNav, navWidgets) } type={"SUB"} />
                    } />

                    <Route exact path="/widgets/whisper" component={() =>
                        <WidgetBoard nav={ enter(whisperWidgets, allWidgetsNav, navWidgets) } type={"WHISPERS"} />
                    } />

                    <Route exact path="/widgets/bits" component={() =>
                        <WidgetBoard nav={ enter(bitsWidgets, allWidgetsNav, navWidgets) } type={"BITS"} />
                    } />

                    /* External access widgets */

                    <Route exact path="/widget/:token/user/:userId" component={ OBSWidget } />

				</Switch>
			</BrowserRouter>
		);
	}
}

const enter = (navigationItem, all, nav) => {
    all.forEach(item => {
    	item.isActive = false;
	});

    navigationItem.isActive = true;

    return nav;
};

ReactDOM.render(
	<Index />,
	document.getElementById("application")
);
