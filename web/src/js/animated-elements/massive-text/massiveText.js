import React from "react";
import TextLoop from "react-text-loop";

class MassiveText extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<h1 className="wrapper" style={this.props.style}>
				<TextLoop interval={3000} mask={true}>
					{this.props.phrases.map((phrase) => <span className="letter">{phrase}</span>)}
				</TextLoop>
			</h1>
		);
	}
}

export default MassiveText;