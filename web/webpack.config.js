/* eslint-disable */

const HtmlWebPackPlugin = require("html-webpack-plugin");
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const relPath = "/build";

module.exports = {
	entry: [
		"./src/js/index.js"
	],
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: /node_modules/,
				use: ["babel-loader"]
			},
			{
				test: /\.css$/,
				use: ["style-loader", "css-loader"]
			},
			{
				test: /\.(scss|sass)$/,
				use: [
					{ loader : "style-loader" },
                    {
                    	loader : "css-loader",
						options: {
                            url: true
                        }
                    },
                    { loader : "scss-loader" }
				],
			},
			{
				test: /\.(gif|png|jpe?g|svg|ico)$/i,
				use: [
					"file-loader",
					{
						loader: "image-webpack-loader",
						options: {
							bypassOnDebug: true,
							disable: true
						},
					}
				],
			},
            {
                test: /\.(mp3|ogg)$/i,
                use: [
                    "file-loader"
                ],
            },
			{
				test: require.resolve("jquery"),
				use: [
					{ loader: "expose-loader", options: "jQuery" },
					{ loader: "expose-loader", options: "$" }
				]
			}
		]
	},
	node: {
		fs: 'empty'
	},
	resolve: {
		extensions: ["*", ".js", ".jsx"]
	},
	devServer: {
		historyApiFallback: true,
        contentBase: __dirname + '/build'
	},
	plugins: [
        new HtmlWebPackPlugin({
            template: "./src/index.html",
            filename: "./index.html"
        })
    ],
	output: {
		path: __dirname + relPath,
		publicPath: "/build/",
		filename: "bundle.js"
	}
};

/* eslint-enable */
